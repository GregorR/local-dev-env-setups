# Setup Tricks in Your Local Area Looking for Action

## Executing Shell Setups
Find the install script beginning `install.[...]` for you desired shell under `./shelling/<SHELL>` and
execute it in the targeted shell. Please do not move the script.<br>
Example: 
```PowerShell
PS> .\shelling\ms-powershell\install.ps1
```

### MS PowerShell
Installs modules
- VirtualEnvWrapper
  - eases virtualenv management for python with plain pip and setuptools
  - run `VirtualEnvWrapper` for more info after installation
  - optionally, set your desired default python executable under env var VIRTUALENVWRAPPER_PYTHON. otherwise
  defaults to `python` if no python executable is provided as command line option.
  - set env var WORKON_HOME to the central directory for where to put you virtual envs. defaults to
  <USERPROFILE/Envs>.
- posh-git
  - get nice auto-completions via simple tab for git
  - frozen version of https://github.com/dahlbyk/posh-git#installation
- KubectlCompletion
  - get auto-completions via tab for your `kubectl` command-line tool
  - `kubectl` comes automatically with your install of the openshift cli tool `oc`
  - `kubectl` can do almost everything `oc` can

Further, installs some aliases and PowerShell settings. See your PS `$profile` after installation.
