# Install VirtualEnvWrapper tailored to MS PowerShell

$PowerShellProfile = $PROFILE
$PowerShellPath = Split-Path $PowerShellProfile
$PSModulesDir = Join-Path $PowerShellPath Modules
$AssetsDir = Join-Path (Split-Path $MyInvocation.MyCommand.Path) assets

function Binary-Prompt ($Message) {
    Do {
        $Key = (Read-Host "$Message [Y/n]").ToLower()
        if (!$Key) {$Key = "y"}
    } While ($Key -ne "y" -And $Key -ne "n")

    return $Key
}

function Prompt ($Message) {
    $Key = (Read-Host "$Message").ToLower()
    return $Key
}

# create ~/Documents/WindowsPowerShell/Modules if doesn't exist
if (!(Test-Path $PSModulesDir))
{
    Write-Host "Creating directory: $PSModulesDir."
    New-Item -ItemType Directory -Force -Path $PSModulesDir
}

# If Powershell profile doesn't exist, create
if (!(Test-Path $PowerShellProfile)){
    Write-Host "Creating missing PS profile at $PowerShellProfile."
    New-Item -ItemType File -Path $PowerShellProfile
    Write-Host
}
else {
    Add-Content -Path $PowerShellProfile -Value "# Following content added automatically by grddy's install script."
}

# Copy Modules and Order Import
$ModuleNames = New-Object string[] 3
$ModuleNames[0] = "VirtualEnvWrapper"
$ModuleNames[1] = "posh-git"
$ModuleNames[2] = "KubectlCompletion"
ForEach ($Name in $ModuleNames) {
    if (Test-Path $PSModulesDir\$Name) {
        $WriteModule = Binary-Prompt "A folder named $Name already exists in $PSModulesDir. Overwrite?"
    }
    else {
        $WriteModule = $true
    }

    if ($WriteModule) {
        Write-Host "Installing $Name module."
        Copy-Item -Recurse -Force -Path $AssetsDir\Modules\$Name -Destination $PSModulesDir\
        $Line = "Import-Module $Name"
        if (!(Select-String -SimpleMatch $Line -Path $PowerShellProfile)) {
            Add-Content -Path $PowerShellProfile -Value $Line
        }
    }
}

# Copy lines if not already present
$NewContent = Get-Content -Path $AssetsDir\"Microsoft.PowerShell_profile.ps1"
if ($NewContent) {
    Write-Host "Adding aliases and configs to $PowerShellProfile."
    Add-Content -Path $PowerShellProfile -Value ""
    ForEach ($Line in $NewContent) {
        if ($Line -And !(Select-String -SimpleMatch $Line -Path $PowerShellProfile)) {
            Add-Content -Path $PowerShellProfile -Value $Line
        }
    }
}


try {
    $PythonCommand = Get-Command "python.exe"
    if (!("$PythonCommand" -eq "python.exe")) {
        Write-Host "WARNING: make sure you have command 'python.exe' linked
        to your desired default python executable."
    }
}
catch [CommandNotFoundException] {
    Write-Host "WARNING: python command 'python.exe' not found."
}
.$PowerShellProfile
Write-Host "Finished installing PowerShell amenities."
