# Shows navigable menu of all options when hitting Tab
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Autocompletion for arrow keys
Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

# Convenience functions and aliases
function gpp { git pull -p --all $args }
function gs { git status $args }
function gb { git branch $args }
function gba { git branch -a $args }
function gch { git checkout $args }
function Git-Log-Sexy { git log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit }
function Git-Add-Updated { git add -u $args }
function Git-Commit-Amend-Noedit { git commit --amend --no-edit $args }
function docker-clean-all { (docker stop $(docker container ls -a -q)) -and (docker system prune -a -f --volumes) }
function which($name){ Get-Command $name | Select-Object -ExpandProperty Definition }


Set-Alias -Name gls -Value Git-Log-Sexy
Set-Alias -Name gau -Value Git-Add-Updated
Set-Alias -Name gca -Value Git-Commit-Amend-Noedit

