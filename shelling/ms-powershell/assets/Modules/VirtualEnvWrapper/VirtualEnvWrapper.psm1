﻿#
# Python virtual env manager inspired by VirtualEnvWrapper
#
# Copyright (c) 2017 Regis FLORET
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

$WORKON_HOME=$Env:WORKON_HOME
$VIRTUALENVWRAPPER_PYTHON=""
$VIRTUALENVWRAPPER_HOOK_DIR=""
$Version = "0.2.0"

# Set the default path and create the directory if don't exist
if (!$WORKON_HOME) {
    $WORKON_HOME = "$Env:USERPROFILE\Envs"
}

if ((Test-Path $WORKON_HOME) -eq $false) {
    mkdir $WORKON_HOME
}


# Get the absolute path for the environment
function Get-FullPyEnvPath($pypath) {
    return ("{0}\{1}" -f $WORKON_HOME, $pypath)
}


# Display a formatted error message
function Write-FormattedError($err) {
    Write-Host
    Write-Host "  ERROR: $err" -ForegroundColor Red
    Write-Host
}


# Display a formatted success message
function Write-FormattedSuccess($msg) {
    Write-Host
    Write-Host "  $msg" -ForegroundColor Green
    Write-Host
}

# Retrieve the python version from the path to the python exe.
# Return the major version of python
function Get-PythonVersion($Python) {

    $python_version = Invoke-Expression "& '$Python' --version 2>&1"
    if ($Python -and !$python_version) {
        Write-FormattedError "Can't get version from given python cmd: '$Python'."
        return
    }

    $is_version_3 = $python_version -match "^Python\s3"
    if (!$is_version_3) {
        Write-FormattedError "Unexpected python version: $python_version"
        return
    }

    return "3"
}

# Check if there is an environment named $Name
function PyEnvExists($Name) {
    $children = Get-ChildItem $WORKON_HOME

    if ($children.Length -gt 0) {
        for ($i=0; $i -lt $children.Length; $i++) {
            if (([string]$children[$i]).CompareTo($Name) -eq 0) {
                return $true
            }
        }
    }

    return $false
}

# Test if given or any virtual env is active
function Get-IsInPythonVenv($Name) {
    if ($Env:VIRTUAL_ENV) {
        if ($Name) {
            if (([string]$Env:VIRTUAL_ENV).EndsWith($Name)) {
                return $true
            }

            return $false;
        }

        return $true
    }

    return $false
}

# Now, work on an env
function Workon {
    Param([string] $Name)

    if (!$Name) {
        Write-FormattedError "No env name given, is required to select the env to work on."
        return
    }

    $new_pyenv = Get-FullPyEnvPath $Name
    if ((Test-Path $new_pyenv) -eq $false) {
        Write-FormattedError "Python environment '$Name' doesn't exist. You can create one via 'MkVirtualEnv $Name'"
        return
    }

    if (Get-IsInPythonVenv -eq $true) {
        deactivate
    }

    $VEnvScriptsPath = Join-Path $new_pyenv "Scripts"
    $ActivatePsPath = Join-Path $VEnvScriptsPath "activate.ps1"
    if ((Test-path $ActivatePsPath) -eq $false) {
        Write-FormattedError "Unable to find PS activation script '$ActivatePsPath'"
        return
    }

    . $ActivatePsPath
    $Env:VIRTUAL_ENV = "$new_pyenv"
}

# Common command to create the Python Virtual Environment.
# $Command contains either the Py2 or Py3 command
function Invoke-CreatePyEnv($Command, $Name) {
    $NewEnv = Join-Path $WORKON_HOME $Name
    Write-Host "Creating virtual env... "

    Invoke-Expression "$Command '$NewEnv'"
    Workon $Name
    Write-FormattedSuccess "Virtual env $Name created and activated. Feliz navidad, muchacho."
}


# Get defaults if no python command given.
function Get-Python ($Python) {

    # No path given, get a default
    if (!$Python) {
        if ("$VIRTUALENVWRAPPER_PYTHON") {
            $Python = $VIRTUALENVWRAPPER_PYTHON
        }
        else {
            try {$Python = (Get-Command "python.exe" | Select-Object -ExpandProperty Source)}
            catch [CommandNotFoundException] {
                $Python = (Get-Command "python" | Select-Object -ExpandProperty Source)
            }
            catch [CommandNotFoundException] {
                $Python = (Get-Command "python39" | Select-Object -ExpandProperty Source)
            }
        }
    }
    return $Python
}

# Create Python Environment using the venv module
function New-Python3Env($Python, $Name) {

    $Command = "& '$Python' -m venv"

    Invoke-CreatePyEnv $Command $Name
}

# Create a Python Environment
function New-PythonEnv($Python, $Name, $Packages, $Append) {
    $version = Get-PythonVersion $Python

    $BackupPath = $Env:PYTHONPATH
    if ($Append) {
        $Env:PYTHONPATH = "$Append;$($Env:PYTHONPATH)"
    }

    if ("$Version" -eq "3") {
        New-Python3Env -Python $Python -Name $Name
    } else {
        Write-FormattedError "ValueError: unexpected python version: $Version"
    }

    $Env:PYTHONPATH = $BackupPath
}

# Create a new virtual environment.
function New-VirtualEnv()
{
    Param(
        [Parameter(HelpMessage="Virtual-env name")]
        [string]$Name,

        [Parameter(HelpMessage="Requirements file")]
        [alias("r")]
        [string]$Requirements,

        [Parameter(HelpMessage="Command that invokes desired python executable.")]
        [alias("p")]
        [string]$Python,

        [Parameter(HelpMessage="Package(s) to install. Repeat the parameter for more than one.")]
        [alias("i")]
        [string[]]$Packages,

        [Parameter(HelpMessage="Associate an existing project directory with the new environment.")]
        [alias("a")]
        [string]$Associate
    )

    <#
    .Synopsis
        Create a new virtual environment.
    #>

    if ($Name.StartsWith("-")) {
        Write-FormattedError "The virtual environment name cannot begin with the literal '-' (hyphen)"
        return
    }

    if ($Append -and !(Test-Path $Append)) {
        Write-FormattedError "The path '$Append' doesn't exist"
        return
    }

    if (!$Name) {
        Write-FormattedError "You must provide a name for the desired virtual env."
        return
    }

    if ((PyEnvExists $Name) -eq $true) {
        Write-FormattedError "An environment with the same name already exists."
        return
    }

    $Python = Get-Python $Python
    New-PythonEnv -Python $Python -Name $Name

    foreach($Package in $Packages)  {
         Invoke-Expression "$WORKON_HOME\$Name\Scripts\python.exe -m pip install $Package"
    }


    if ($Requirements -ne "") {
        if (! $(Test-Path $Requirements)) {
            Write-Error "Requirements file '$Requirements' doesn't exist"
            Break
        }
        Invoke-Expression "$WORKON_HOME\$Name\Scripts\python.exe -m pip install -r $Requirements"
    }
}


function Get-VirtualEnvs {
    $children = Get-ChildItem $WORKON_HOME
    Write-Host
    Write-Host "`tPython virtual environments available"
    Write-Host
    Write-host ("`t{0,-30}{1,-15}" -f "Name", "Python version")
    Write-host ("`t{0,-30}{1,-15}" -f "====", "==============")
    Write-Host

    if ($children.Length) {
        $failed = [System.Collections.ArrayList]@()

        for($i = 0; $i -lt $children.Length; $i++) {
            $child = $children[$i]
            try {
                $PythonVersion = (((Invoke-Expression ("$WORKON_HOME\{0}\Scripts\Python.exe --version 2>&1" -f $child.name)) -replace "`r|`n","") -Split " ")[1]
                Write-host ("`t{0,-30}{1,-15}" -f $child.name,$PythonVersion)
            } catch {
                $failed += $child
            }
        }
    } else {
        Write-Host "`tNo Python Environments"
    }
    if ($failed.Length -gt 0) {
        Write-Host
        Write-Host "`tAdditionally, one or more environments could not be listed"
        Write-Host "`t=========================================================="
        Write-Host
        foreach ($item in $failed) {
            Write-Host "`t$item"
        }
    }

    Write-Host
}

# Remove a virtual environment.
function Remove-VirtualEnv {
    Param(
        [string]$Name
    )

    if ((Get-IsInPythonVenv $Name) -eq $true) {
        Write-FormattedError "You want to remove the Virtual Env you are in. Please deactivate beforehand."
        return
    }

    if (!$Name) {
        Write-FormattedError "You must provide an environment name."
        return
    }

    $full_path = Get-FullPyEnvPath $Name
    if ((Test-Path $full_path) -eq $true) {
        Remove-Item -Path $full_path -Recurse
        Write-FormattedSuccess "$Name was deleted permanently."
    } else {
        Write-FormattedError "$Name not found."
    }
}

<#
.Synopsis
    Get the current version of VirtualEnvWrapper
#>
function Get-VirtualEnvVersion() {
    Write-Host "Version $Version"
}


<#
.Synopsis
    Display info about this module.
#>
function VirtualEnvWrapper() {
    Write-Host """Create, delete, view virtual python envs for arbitrary python-3 versions.

    Usage:
    ======

    'mkvirtualenv' <Name> to create an env
    'lsvirtualenv' to list available envs
    'rmvirtualenv' <Name> to remove given env

    Use Get-Help <FUNCTION> for more info.

    """
}

# Powershell aliases for naming conventions
Set-Alias lsvirtualenv Get-VirtualEnvs
Set-Alias rmvirtualenv Remove-VirtualEnv
Set-Alias mkvirtualenv New-VirtualEnv

Write-Host "General Kenobi!"
